# Scripts from others

Scripts written by other people which I find useful.

Some may have been modified by me. The first commit in which they appear
in this repository will be the original unmodified version of the script.

## screen_ssh.sh

By Chris Jones, obtained from this [repository](https://code.launchpad.net/~cmsj/+junk/screen_ssh).
License: GPLv2

Set the title of the current screen to the hostname being ssh'd to.

The version in this repo contains a modification by Stefhen Hovland,
obtained from a GitHub [gist](https://gist.github.com/res0nat0r/221750).

This is intended to be called by ssh(1) as a LocalCommand.
For example, put this in ~/.ssh/config:

```
Host *
  LocalCommand /path/to/screen_ssh.sh $PPID %n
```

## set-commit-author-hook

By Daniel Vrátil, obtained from his [blog](http://www.dvratil.cz/2015/12/git-trick-628-automatically-set-commit-author-based-on-repo-url/).
License: GPL (which version?)

A post-checkout hook that will check the value of user.email
in .git/config and set it to whatever we want based on URL of the
“origin” remote.

To install it, just copy the script above to
~/.git-templates/hooks/post-checkout, make it executable and run

```
git config --global init.templatedir ~/.git-templates
```

All hooks from templatedir are automatically copied into .git/hooks
when a new repository is created (git init or git clone) – this way
the hook will get automatically deployed to every new repo.

# udfhd.pl

By Pieter Wuille, obtained from his [website](http://sipa.ulyssis.org/software/scripts/udf-harddisk/).
License: GPLv3+

Partition and format a hard disk (or USB key) using UDF.
